# Bowowow #

This is a thing to automatically load dependencies from Bower in to an HTML document without any manual work. Simply install something on the command line and it should auto load.

This is by no means a proper 'from the specs' app. I wrote it based upon what I was using from Bower so it is possible a package could appear that does not work with it.

### Usage ###

It uses a sort of singleton style thing to make life easier in terms of dealing with instantiation.  

To get an instance use:

```
#!php

\Bowowow\Bower::i()
```

Since the above returns an object you can access methods in the usual PHP way.  The only really useful one is:


```
#!php

Bowowow\Bower::i()->load($type)
```

Which will return an array of paths to the files you need to include. For example:


```
#!php

foreach (Bowowow\Bower::i()->load('css') as $file) {
    echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"$file\" />". PHP_EOL. "\t";
}

foreach (Bowowow\Bower::i()->load('js') as $file) {
    echo "<script type=\"text/javascript\" src=\"$file\"></script>". PHP_EOL. "\t";
}
```


To load the CSS and PHP.

### License ###

It's BSD. Do what you want.

### Contribution Guidelines ###

If you can fix anything go for it!