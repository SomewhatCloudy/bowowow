<?php
namespace Bowowow;

/**
 *
 */
abstract class Base
{
	static
		$modules = [];

	/**
	 * @return	static
	 */
	static public function i ()
	{
		$parts = explode('\\', get_called_class());
		$name = array_pop($parts);

		$called = get_called_class();

		if (isset(self::$modules[$called]))
		{
			return self::$modules[$called];
		}

		self::$modules[$called] = new $called();

		return self::$modules[$called];
	}
}