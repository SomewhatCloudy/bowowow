<?php
namespace Bowowow;

/**
 * Custom Exception
 *
 * Adds support for a code that is not an integer
 */
class Exception extends \Exception
{
	/**
	 * Constructor
	 *
	 * @param string $message - Message to display
	 * @param bool $code - Error code as a string
	 * @param Exception $previous - Previous Exception
	 */
	public function __construct ($message, $code=false, Exception $previous=null)
	{
		parent::__construct($message, false, $previous);
		$this->code = $code;
	}
}