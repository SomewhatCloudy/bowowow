<?php
namespace Bowowow;

/**
 *
 */
class Bower extends Base
{
	protected
		$bower_path = 'bower_components',
		$includes = null,
		$json = [];

	/**
	 *
	 */
	public function set_bower_path ($bower_path)
	{
		$this->bower_path = $bower_path;
	}

	/**
	 *
	 */
	public function load ($type, $exclude=[])
	{
		$found = [];
		$components = $this->all();

		1/1;

		foreach ($components as $key=>$component)
		{
			if (!in_array($key, $exclude))
			{
				foreach ($component as $file)
				{
					if ($file['type'] === $type)
					{
						$found[] = $file['path'];
					}
				}
			}
			else
			{
				1/1;
			}
		}

		return $found;
	}

	/**
	 * Get path to all includes
	 *
	 * @param	boolean	$flatten - Return a numeric keyed array rather than an associative one.
	 * @param	string	$base_path - Relative path off of site root that the components directory is in.
	 * @return	array	Returns an array of file paths.
	 */
	protected function all ($base_path='')
	{
		// Cached?
		if ($this->includes !== null)
		{
			return $this->includes;
		}

		// Not cached
		$components = $this->components($base_path);
		$includes = [];

		foreach ($components as $component)
		{
			$includes = array_merge($includes, $this->single($component['name'], $base_path));
		}

		$this->includes = $includes;

		return $includes;
	}

	/**
	 * Get bower components.
	 *
	 * Get the bower file that describes the addon
	 *
	 * @param	string		$base_path - Specify path to repositories
	 * @param	boolean		$refresh - Refresh the currently loaded module list
	 * @return	array		Returns the package info file.
	 */
	protected function components ($base_path = '', $refresh = false)
	{
		$path = $this->get_bower_path(false, $base_path);
		$json = [];
		$loaded = [];

		// No directory!
		if (!realpath($path)) {
			return [];
		}

		$items = scandir($path);

		// Load cached
		if ($this->json && !$refresh)
		{
			return $this->json;
		}

		foreach($items as $item)
		{
			if (substr($item, 0, 1) !== '.' && is_dir($path. $item))
			{
				$loaded = false;
				$path_include = $path. $item. DIRECTORY_SEPARATOR;
				$paths_json = [
					$path_include. 'bower.json',
					$path_include. 'package.json',
					$path_include. 'component.json'
				];

				foreach ($paths_json as $path_json)
				{
					if (file_exists($path_json))
					{
						$loaded = json_decode(file_get_contents($path_json), true);
						break;
					}
				}

				if (!$loaded)
				{
					throw new Exception("Cannot load that include ({$item}). No bower.json/package.json/component.json file found.", 'NOT_BOWER');
				}

				$json[$item] = $loaded;

				// Force add jQuery as a dependency of AngularJS
				if ($json[$item]['name'] == 'angular')
				{
					$json[$item]['dependencies']['jquery'] = '~';
				}
			}
		}

		$this->json = $json;

		return $json;
	}

	/**
	 * Get path to single include.
	 *
	 * Returns an array to take account of dependencies.
	 *
	 * @throws	exception	NOT_BOWER - Cannot find include files.
	 *
	 * @param	string		$name - Name of Bower include (not directory, so angular, no angularjs).
	 * @return	array		Returns an array of file paths.
	 */
	protected function single ($name, $base_path=false)
	{
		$components = $this->components($base_path);
		$includes = [];

		foreach ($components as $path=>$json)
		{
			if ($json['name'] === $name)
			{
				// Dependencies
				if (isset($json['dependencies']))
				{
					$json['dependencies'] = (array) $json['dependencies'];

					foreach ($json['dependencies'] as $dep=>$version)
					{
						$includes = array_merge($includes, $this->single($dep, $base_path));
					}
				}

				// File(s) to include
				if (!isset($json['main']))
				{
					// No main, make a guess!
					$json['main'] = "{$name}.js";
				}

				$json['main'] = (array) $json['main'];

				// Create array
				$includes[$name] = [];

				foreach ($json['main'] as $path_main)
				{
					$includes[$name][] = [
						'type' => pathinfo($this->get_bower_path(false, $base_path). $path_main, PATHINFO_EXTENSION),
						'path' => $this->get_bower_path(true, $base_path). $path. DIRECTORY_SEPARATOR. $path_main
					];
				}
			}
		}

		return $includes;
	}

	/**
	 * Get path to bower repositories
	 */
	protected function get_bower_path ($relative=false, $base_path='')
	{
		if ($base_path)
		{
			if ($relative)
			{
				$path = $base_path. $this->bower_path. DIRECTORY_SEPARATOR;
			}
			else
			{
				$path = $base_path. $this->bower_path. DIRECTORY_SEPARATOR;
			}
		}
		else
		{
			if ($relative)
			{
				$path = $this->bower_path. DIRECTORY_SEPARATOR;
			}
			else
			{
				$path = $this->bower_path. DIRECTORY_SEPARATOR;
			}
		}

		return $path;
	}
}